/*
@D:\Projects\LoSys\Update\unlock_users.sql
*/

alter user ls_user account unlock;
alter user lse account unlock;
alter user dshelepov account unlock;
alter user ls_view account unlock;
alter user ls_astro account unlock;
alter user ls_mdm account unlock;
alter user ls_web account unlock;
alter user ls_web_special account unlock;
alter user ls_im account unlock;
alter user ls_callcenter account unlock;
alter user devino account unlock;
alter user ls_smp account unlock;
alter user sms_rapporto account unlock;
alter user ls_naumen account unlock;
alter user sms_kz1 account unlock;
alter user ls_competition account unlock;
alter user ls_beorg account unlock;
alter user ls_external_services account unlock;
alter user sm_sms account unlock;
alter user ls_wechat account unlock;
alter user ls_wifi_analytic account unlock;
