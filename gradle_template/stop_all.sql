/*
@D:\Projects\LoSys\Update\stop_all.sql;
*/

declare
    procedure p_disable_job
        (
          p_job_owner  varchar2,    -- владелец джоба
          p_job_name   varchar2     -- имя джоба
        )
    as
        v_cnt number(1);
    begin
        -- ищем включенный джоб с таким именем
        select
            count(0)
        into
            v_cnt
        from
            all_scheduler_jobs t
        where 
            t.owner = p_job_owner and
            t.job_name = p_job_name and
            t.ENABLED = 'TRUE' and
            rownum = 1;
        -- если нашли включенным - отключаем
        if v_cnt = 1 then
            sys.dbms_scheduler.disable(name => p_job_owner || '.' || p_job_name);
        end if;
    end;
begin
    rpl.rpl_control.stop_all_job;
    -- Пакеты
    ls.pr_processing.stop_process_scheduler;
    
    -- останавливаем джобы, настроенные в таблице
    ls.sc_job_utils.stop_all_job
    (
        p_wait_end => true,
        p_wait_sec => 10,
        p_timeout  => 120
    );
    
    begin
        execute immediate 'begin mk_process_anketa_paper.stop_process_anketa_paper; end;';
    exception
        when others then
            null;
    end;
    
    tu_init;
    -- остановка всех JOB'ов
    p_disable_job('LS', 'LS_LOAD_DATA_FROM_WL');
    p_disable_job('LS', 'LS_ANKETA_PAPER_MAIN_JOB');
    p_disable_job('LS', 'LS_SCHED_ACTION_MAIN_JOB');
    p_disable_job('LS', 'LS_EVENT_BURN_BONUS_JOB');
    p_disable_job('LS', 'EMAIL_PROCESSING');
    p_disable_job('LS', 'LS_SMS_CONTROL');
    p_disable_job('LS', 'LS_YANDEX_MAPS');
    p_disable_job('LS', 'PC_LOG_GUARD');
    p_disable_job('LS', 'CALC_DATA_FOR_REPORTS');
    p_disable_job('LS', 'LS_MK_CSM_TASK_ESCALATION');
end;
/
