truncate table ls_api_log_p;
truncate table ls_api_log;
truncate table js_journal;
declare
    v_date date;
begin
    for x in 
    (
        select
            partition_name,
            high_value
        from
            user_tab_partitions
        where
            table_name = 'LOG_CALL_INFO' and
            partition_position > 1
        order by
            partition_position
    )
    loop
        v_date := to_date(regexp_substr(x.high_value,'[-0-9]+'), 'yyyy-mm-dd');
        if v_date < sysdate - interval '3' month then
            execute immediate 'alter table log_call_info drop partition ' || x.partition_name;
        end if;
    end loop;
end loop;
/
alter index log_call_info_pk rebuild online;
