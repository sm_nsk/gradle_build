set echo on
prompt ==========================================================================================
prompt Установка AFTER скриптов
prompt ==========================================================================================
---!!! rpl
begin
    for x in (select * from table(rpl.rpl_control.get_grants_list))
    loop
        execute immediate x.column_value;
    end loop;
    rpl.rpl_control.recreate_own_objects;
end;
/
---!!! recompile
begin
 ut.ut_compile_invalid_objects('RPL');
 ut.ut_compile_invalid_objects('LS');
end;
/

select * from table(ut.ut_get_invalid_objects('RPL'))
union all
select * from table(ut.ut_get_invalid_objects('LS'));

@@unlock_users.sql;
@@start_all.sql;

spool off