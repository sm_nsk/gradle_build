/*
@D:\Projects\LoSys\Update\lock_users.sql
*/

begin
    for i in 1 .. 10
    loop
        for r in
            (
                select
                    *
                from
                    v$session
                where
                    Upper(username) in 
                        (
                            'LS_USER',
                            'DSHELEPOV',
                            'LS_VIEW',
                            'LS_ASTRO',
                            'LS_DWH',
                            'LS_MDM',
                            'LS_WEB',
                            'LS_WEB_SPECIAL',
                            'LS_IM',
                            'LS_CALLCENTER',
                            'DEVINO',
                            'LS_SMP',
                            'OLYMP_USER',
                            'SMS_RAPPORTO',
                            'LS_NAUMEN',
                            'SMS_KZ1',
                            'LS_COMPETITION',
                            'LS_BEORG',
                            'LS_EXTERNAL_SERVICES',
                            'SM_SMS',
                            'LS_WECHAT',
                            'LS_WIFI_ANALYTIC'
                        )
            )
        loop
            begin
                execute immediate 'alter system kill session ''' || r.sid || ', ' || r.serial# || ''' immediate';
            exception
                when others then
                    null;
            end;
        end loop;
    end loop;
    -- подождем 2 секунды
    dbms_lock.sleep(seconds => 2);
end;
/

-- Блокируем пользователей
alter user ls_user account lock;
alter user dshelepov account lock;
alter user ls_view account lock;
alter user ls_astro account lock;
alter user ls_mdm account lock;
alter user ls_web account lock;
alter user ls_web_special account lock;
alter user ls_im account lock;
alter user ls_callcenter account lock;
alter user devino account lock;
alter user ls_smp account lock;
alter user sms_rapporto account lock;
alter user ls_naumen account lock;
alter user sms_kz1 account lock;
alter user ls_competition account lock;
alter user ls_beorg account lock;
alter user ls_external_services account lock;
alter user sm_sms account lock;
alter user ls_wechat account lock;
alter user ls_wifi_analytic account lock;

-- Отстреливаем все сессии этих пользователей
begin
    for i in 1 .. 10
    loop
        for r in
            (
                select
                    *
                from
                    v$session
                where
                    Upper(username) in 
                        (
                            'LS_USER',
                            'DSHELEPOV',
                            'LS_VIEW',
                            'LS_ASTRO',
                            'LS_DWH',
                            'LS_MDM',
                            'LS_WEB',
                            'LS_WEB_SPECIAL',
                            'LS_IM',
                            'LS_CALLCENTER',
                            'DEVINO',
                            'LS_SMP',
                            'OLYMP_USER',
                            'SMS_RAPPORTO',
                            'LS_NAUMEN',
                            'SMS_KZ1',
                            'LS_COMPETITION',
                            'LS_BEORG',
                            'LS_EXTERNAL_SERVICES',
                            'SM_SMS',
                            'LS_WECHAT',
                            'LS_WIFI_ANALYTIC'
                        )
            )
        loop
            begin
                execute immediate 'alter system kill session ''' || r.sid || ', ' || r.serial# || ''' immediate';
            exception
                when others then
                    null;
            end;
        end loop;
    end loop;
    -- подождем 2 секунды
    dbms_lock.sleep(seconds => 2);
end;
/
