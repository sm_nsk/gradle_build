/*
@D:\Projects\LoSys\Update\start_all.sql;
*/

declare
    procedure p_enable_job
        (
          p_job_owner  varchar2,    -- владелец джоба
          p_job_name   varchar2     -- имя джоба
        )
    as
        v_cnt number(1);
    begin
        -- ищем выключенный джоб с таким именем
        select
            count(0)
        into
            v_cnt
        from
            all_scheduler_jobs t
        where 
            t.owner = p_job_owner and
            t.job_name = p_job_name and
            t.ENABLED = 'FALSE' and
            rownum = 1;
        -- если нашли выключенным - включаем
        if v_cnt = 1 then
            sys.dbms_scheduler.enable(name => p_job_owner || '.' || p_job_name);
        end if;
    end;
begin
    rpl.rpl_control.start_all_job;
    -- Пакеты
    pr_processing.start_process_scheduler;
    mk_process_anketa_paper.start_process_anketa_paper;
    
    -- включаем все джобы
    sc_job_utils.start_all_job; 
    /* как вариант можно включить только разрешенные для запуска джобы
      sc_job_utils.start_all_enable_job;    
    */
        
    -- Включаем JOB'ы
    p_enable_job('LS', 'LS_LOAD_DATA_FROM_WL');
    p_enable_job('LS', 'LS_ANKETA_PAPER_MAIN_JOB');
    p_enable_job('LS', 'LS_SCHED_ACTION_MAIN_JOB');
    p_enable_job('LS', 'LS_EVENT_BURN_BONUS_JOB');
    p_enable_job('LS', 'EMAIL_PROCESSING');
    p_enable_job('LS', 'LS_SMS_CONTROL');
    p_enable_job('LS', 'LS_YANDEX_MAPS');
    p_enable_job('LS', 'PC_LOG_GUARD');
    p_enable_job('LS', 'CALC_DATA_FOR_REPORTS');
    p_enable_job('LS', 'LS_MK_CSM_TASK_ESCALATION');
end;
/
